<?php

namespace App\Connection;

use PDO;

class BaseB
{

    protected static $con;

    public function __construct()
    {
        $db_host    = "localhost";
        $db_nome    = "base_b";
        $db_usuario = "root";
        $db_senha   = "";
        $db_driver  = "mysql";

        try {
            $db = new PDO("$db_driver:host=$db_host; dbname=$db_nome", $db_usuario, $db_senha);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$con = $db;
        } catch (PDOException $e) {
            die("Connection Error: " . $e->getMessage());
        }
    }

    /**
     * Retorna o objeto PDO conectado com o banco de dados
     * @return PDO
     */
    public static function getConnection()
    {
        if (!self::$con){
            new BaseB();
        }

        return self::$con;
    }

    /**
     * Busca todas os CPF's registrados na base A da tabela pessoa
     * @return array
     */
    public static function searchAllRegisteredPeople() {
        $pdo = self::getConnection();

        $sql = 'SELECT * 
	          FROM tb_pessoa
         LEFT JOIN tb_endereco
                ON tb_endereco.ID = tb_pessoa.ENDERECO_ID';

        $consulta = $pdo->query($sql);

        return $consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Retorna os bens de uma pessoa
     * @param $pessoaId
     *
     * @return array
     */
    public static function searchPersonPropertyList($pessoaId) {
        $pdo = self::getConnection();

        $sql = 'SELECT * 
	              FROM tb_bens
                 WHERE PESSOA_ID = :pessoaid';

        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':pessoaid', $pessoaId);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    /**
     * Retorna as fontes de renda de uma pessoa
     * @param $pessoaId
     *
     * @return array
     */
    public static function searchPersonSourcesOfIncome($pessoaId) {
        $pdo = self::getConnection();

        $sql = 'SELECT * 
	              FROM tb_fonte_renda
                 WHERE PESSOA_ID = :pessoaid';

        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':pessoaid', $pessoaId);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

}