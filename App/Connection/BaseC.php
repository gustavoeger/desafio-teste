<?php

namespace App\Connection;

use PDO;

class BaseC
{

    protected static $con;

    public function __construct()
    {
        $db_host    = "localhost";
        $db_nome    = "base_c";
        $db_usuario = "root";
        $db_senha   = "";
        $db_driver  = "mysql";

        try {
            $db = new PDO("$db_driver:host=$db_host; dbname=$db_nome", $db_usuario, $db_senha);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$con = $db;
        } catch (PDOException $e) {
            die("Connection Error: " . $e->getMessage());
        }
    }

    /**
     * Retorna o objeto PDO conectado com o banco de dados
     * @return PDO
     */
    public static function getConnection()
    {
        if (!self::$con){
            new BaseC();
        }

        return self::$con;
    }

    /**
     * Retorna todas as movimentações financeiras de um CPF
     * @param $cpf
     *
     * @return array
     */
    public static function getAllFinancialMovements($cpf) {
        $pdo = self::getConnection();

        $sql = 'SELECT tb_movimentacao.PESSOA_ID, tb_movimentacao.DATA_MOVIMENTACAO, tb_movimentacao.TIPO, tb_grupo.DESCRICAO as GRUPO_DESCRICAO,  
                       tb_movimentacao.DESCRICAO as MOVI_DESCRICAO, tb_movimentacao.VALOR
	              FROM tb_movimentacao
            INNER JOIN tb_pessoa
                    ON tb_pessoa.ID = tb_movimentacao.PESSOA_ID
            INNER JOIN tb_grupo
                    ON tb_grupo.ID = tb_movimentacao.GRUPO_ID                    
                 WHERE tb_pessoa.CPF like :cpf';

        $sqlCpf = "%".$cpf."%";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':cpf', $sqlCpf);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Busca a última consulta em bureau de determinada pessoa
     * @param $pessoaId
     *
     * @return mixed
     */
    public static function searchLastQueryInCpf($pessoaId) {
        $pdo = self::getConnection();

        $sql = 'SELECT * 
                  FROM ultimaconsulta
                 WHERE PESSOA_ID = :pessoaid';

        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':pessoaid', $pessoaId);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Busca a última compra com cartão de crédito de determinada pessoa
     * @param $pessoaId
     *
     * @return mixed
     */
    public static function searchLastCreditCardPurchase($pessoaId) {
        $pdo = self::getConnection();

        $sql = 'SELECT * 
                  FROM ultimacompracartao
                 WHERE PESSOA_ID = :pessoaid';

        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':pessoaid', $pessoaId);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

}