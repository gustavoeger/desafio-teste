# desafio-teste

*_Requisitos para a utilização da aplicação:_*
* Servidor Apache instalado
* Composer instalado
* Conter as bases A, B e C criados no seu banco de dados MySQL

Atendendo aos requisitos bastar clonar o projeto na pasta onde o apache está configurado, e
executar o comando *composer install* na pasta raíz do projeto.
