<html>
    <head>
        <meta charset="utf-8" />
        <title>Desafio Teste</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
    </head>
    <body>
        <section class="index-feature">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-xs-12">
                        <div class="system">
                            <h3>Sistema 1</h3>
                            <p>Acessa os dados da base A.</p>
                            <a href="/desafio-teste/sistema1.php" class="execute">Buscar dados</a>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-xs-12">
                        <div class="system">
                            <h3>Sistema 2</h3>
                            <p>Acessa dados da base B para cálculo do Score.</p>
                            <a href="/desafio-teste/sistema2.php" class="execute">Buscar dados</a>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-xs-12">
                        <div class="system">
                            <h3>Sistema 3</h3>
                            <p>Rastrea eventos relacionados a um determinado CPF.</p>
                            <a href="/desafio-teste/sistema3.php" class="execute">Buscar dados</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script src="js/jquery.js"></script>s
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>