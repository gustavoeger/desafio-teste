<?php
require_once '../vendor/autoload.php';

use App\Connection\BaseA;

if($_SERVER['REQUEST_METHOD'] == 'GET') {
    $result = BaseA::searchAllRegisteredPeople();

    $arrPessoas = [];
    foreach($result as $indice => $pessoa) {
        $arrPessoas[$indice] = ['cpf'      => $pessoa['CPF'],
                                'nome'     => $pessoa['NOME'],
                                'endereco' => ['cep'        => $pessoa['CEP'],
                                               'logradouro' => $pessoa['LOGRADOURO'],
                                               'numero'     => $pessoa['NUMERO'],
                                               'bairro'     => $pessoa['BAIRRO'],
                                               'cidade'     => $pessoa['CIDADE'],
                                               'estado'     => $pessoa['ESTADO']]];

        $arrPessoas[$indice]['dividas'] = [];
        $resultDividas = BaseA::searchDebtsOfPerson($pessoa['ID']);
        foreach($resultDividas as $divida) {
            $arrPessoas[$indice]['dividas'][] = ['descricao' => $divida['DESCRICAO'],
                                                 'valor'     => number_format($divida['VALOR'], 2, ',', '.')];
        }
    }
    die(json_encode($arrPessoas));
}

die(json_encode([]));
