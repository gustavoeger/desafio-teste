<?php
require_once '../vendor/autoload.php';

use App\Connection\BaseB;

if($_SERVER['REQUEST_METHOD'] == 'GET') {
    $result = BaseB::searchAllRegisteredPeople();

    $arrPessoas = [];
    foreach($result as $indice => $pessoa) {
        $arrPessoas[$indice] = ['cpf'      => $pessoa['CPF'],
                                'idade'    => $pessoa['IDADE'],
                                'endereco' => ['cep'        => $pessoa['CEP'],
                                               'logradouro' => $pessoa['LOGRADOURO'],
                                               'numero'     => $pessoa['NUMERO'],
                                               'bairro'     => $pessoa['BAIRRO'],
                                               'cidade'     => $pessoa['CIDADE'],
                                               'estado'     => $pessoa['ESTADO']]];

        $arrPessoas[$indice]['bens'] = [];
        $resultBens = BaseB::searchPersonPropertyList($pessoa['ID']);
        foreach($resultBens as $bem) {
            $arrPessoas[$indice]['bens'][] = ['tipo'      => $bem['TIPO'],
                                              'descricao' => $bem['DESCRICAO']];
        }

        $arrPessoas[$indice]['fontes_renda'] = [];
        $resultBens = BaseB::searchPersonSourcesOfIncome($pessoa['ID']);
        foreach($resultBens as $bem) {
            $arrPessoas[$indice]['fontes_renda'][] = ['descricao' => $bem['DESCRICAO']];
        }
    }
    die(json_encode($arrPessoas));
}

die(json_encode([]));
