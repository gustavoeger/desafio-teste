<?php
require_once '../vendor/autoload.php';

use App\Connection\BaseC;

if($_SERVER['REQUEST_METHOD'] == 'POST') {
    $cpf = $_REQUEST['cpf'];

    if(empty($cpf)) {
        die(json_encode([]));
    }

    $resultMovimentacoes = BaseC::getAllFinancialMovements($cpf);
    /** Consideramos que se o CPF não possuir movimentações, também não terá consultas  */
    if(count($resultMovimentacoes)) {
        $arrReturn = ['cpf' => $cpf];
        foreach($resultMovimentacoes as $movimentacao) {
            $dateMovimentacao = date_format(date_create($movimentacao['DATA_MOVIMENTACAO']),"d/m/Y H:i");

            $arrReturn['movi_financeiras'][] = [
                'data'      => $dateMovimentacao,
                'tipo'      => $movimentacao['TIPO'] == 1 ? 'Entrada' : 'Saída',
                'grupo'     => $movimentacao['GRUPO_DESCRICAO'],
                'descricao' => $movimentacao['MOVI_DESCRICAO'],
                'valor'     => number_format($movimentacao['VALOR'], 2, ',', '.')];
        }

        $pessoaId = $resultMovimentacoes[0]['PESSOA_ID'];

        $arrReturn['ultima_consulta'] = [];
        $returnConsulta = BaseC::searchLastQueryInCpf($pessoaId);
        if($returnConsulta) {
            $dateUlltimaConsulta = date_format(date_create($returnConsulta['DATA_CONSULTA']),"d/m/Y H:i");

            $arrReturn['ultima_consulta'][] = ['data'   => $dateUlltimaConsulta,
                                               'bureau' => $returnConsulta['DESCRICAO']];
        }

        $arrReturn['ultima_compra_credito'] = [];
        $returnCompra = BaseC::searchLastCreditCardPurchase($pessoaId);
        if($returnCompra) {
            $dateUlltimaMovimentacao = date_format(date_create($returnCompra['DATA_MOVIMENTACAO']),"d/m/Y H:i");

            $arrReturn['ultima_compra_credito'][] = ['data'      => $dateUlltimaMovimentacao,
                                                     'descricao' => $returnCompra['DESCRICAO'],
                                                     'valor'     => number_format($returnCompra['VALOR'], 2, ',', '.')];
        }

        die(json_encode($arrReturn));
    }
}

die(json_encode([]));