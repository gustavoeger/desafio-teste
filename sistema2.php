<html>
<head>
    <meta charset="utf-8" />
    <title>Desafio Teste</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
</head>
<body>
<section class="systems-feature">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12">
                <a href="/desafio-teste" class="btn btn-block execute">VOLTAR</a>
                <div class="print-result">

                </div>
            </div>
        </div>
    </div>
</section>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>

<script type="text/javascript">
    $(function () {
        $(window).load(function() {
            $.ajax({
                url: "/desafio-teste/servicos/servico2.php",
                type: 'GET',
                success: function(result) {
                    var finalresult = $.parseJSON(result);
                    var html = '';
                    $.each(finalresult, function (index, value) {
                        html += '<div class="panel panel-default">' +
                            '         <div class="panel-heading">' +
                            '            <h4>CPF: '+value.cpf+' - '+value.idade+' anos</h4>' +
                            '        </div>' +
                            '        <div class="panel-body">' +
                            '           <div class="row">' +
                            '               <div class="col-md-4 col-lg-4 col-xs-12">' +
                            '                   <b>Endereço</b> <p></p>' +
                            '                   <p>CEP '+value.endereco.cep+' </p>' +
                            '                   <p>'+value.endereco.logradouro+'</p>' +
                            '                   <p>'+value.endereco.bairro+', '+value.endereco.cidade+'/'+value.endereco.estado+'</p>' +
                            '               </div>';

                        html += '       <div class="col-md-4 col-lg-4 col-xs-12">' +
                            '               <b>Lista de bens</b> ';

                        if(value.bens.length > 0) {
                            html += '            <p>' +
                                '                  <ul> ';
                            $.each(value.bens, function (index,value) {
                                html += '             <li>'+value.tipo+' - '+value.descricao+' </li>'
                            });

                            html += '               </ul>' +
                                '               </p>';
                        } else {
                            html += '<p class="text-muted"> Não possui bens </p>';
                        }
                        html += '       </div>';// col-md-4 col-lg-4 col-xs-12

                        html += '       <div class="col-md-4 col-lg-4 col-xs-12">' +
                            '               <b>Fontes de rendas</b> ';
                        if(value.fontes_renda.length > 0) {
                            html += '            <p>' +
                                '                  <ul> ';

                            $.each(value.fontes_renda, function (index,value) {
                                html += '             <li>'+value.descricao+'</li>'
                            });

                            html += '               </ul>' +
                                '               </p>';
                        } else {
                            html += '<p class="text-muted"> Não possui fontes de rendas </p>';
                        }
                        html += '       </div>';// col-md-4 col-lg-4 col-xs-12

                        html += '       </div>' + //row
                            '        </div>'+ //panel-body
                            '    </div>'; // panel
                    });

                    $('.print-result').html(html);
                }
            });
        });
    });
</script>
</body>
</html>