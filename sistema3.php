<html>
<head>
    <meta charset="utf-8" />
    <title>Desafio Teste</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
</head>
<body>
<section class="systems-feature">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12">
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-lg-offset-4 col-xs12">
                        <div class="jumbotron">
                            <form action="#" class="form-disabled-on-load" id="form-system3">
                                <div class="form-group">
                                    <label for="input-cpf">* Informe um CPF:</label>
                                    <input class="form-control" type="text" id="input-cpf" name="cpf" required="">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-block execute">Buscar</button>
                                </div>
                                <a href="/desafio-teste" class="btn btn-block execute">Voltar</a>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="print-result">

                </div>
            </div>
        </div>
    </div>
</section>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.maskedinput.js"></script>

<script type="text/javascript">
    $(function () {

        $('#input-cpf').mask('999.999.999-99');

        $('body').on('submit','#form-system3',function (ev) {
            ev.preventDefault();

            var cpf = $('#input-cpf').val();

            $.ajax({
                url: "/desafio-teste/servicos/servico3.php",
                type: 'POST',
                data: {cpf: cpf},
                success: function (result) {
                    var finalresult = $.parseJSON(result);
                    var html = '';
                    if(finalresult.cpf) {
                        html += '<div class="panel panel-default">' +
                            '         <div class="panel-heading">' +
                            '             <h4>CPF: '+finalresult.cpf+'</h4>' +
                            '          </div>' +
                            '          <div class="panel-body">' +
                            '               <div class="row">' +
                            '                  <div class="col-md-6 col-lg-6 col-xs-12">' +
                            '                       <b>Movimentações Financeiras</b> <br> <br>';

                        $.each(finalresult.movi_financeiras, function (index, value) {
                            html += '<p>' +
                                '       <b><i>Dia: '+value.data+'</i></b> <br/>' +
                                '        Tipo: '+value.tipo+' - '+value.grupo+'<br/>' +
                                '        '+value.descricao+' - R$ '+value.valor+
                                '    </p>'
                        });

                        html += '             </div>'; //col-md-6 col-lg-6 col-xs-12

                        html += '             <div class="col-md-6 col-lg-6 col-xs-12">'
                        if(finalresult.ultima_consulta.length > 0) {
                            html += '<p>' +
                                '        <b>Última consulta do CPF:</b>' +
                                '        <p>Dia: '+finalresult.ultima_consulta[0].data+'</p>' +
                                '         <p>'+finalresult.ultima_consulta[0].bureau+'</p>' +
                                '    </p>'
                        }

                        if(finalresult.ultima_compra_credito.length > 0) {
                            html += '<p>' +
                                '       <b>Última compra com cartão de crédito:</b>' +
                                '       <p>Dia: '+finalresult.ultima_compra_credito[0].data+'</p>' +
                                '       <p>'+finalresult.ultima_compra_credito[0].descricao+' - R$ '+finalresult.ultima_compra_credito[0].valor+'</p>' +
                                '   </p>';
                        }
                        html += '             </div>' + //col-md-6 col-lg-6 col-xs-12
                        '                 </div>' + //row
                        '             </div>' + //panel-body
                        '         </div>'; //panel

                        $('.print-result').html(html);
                    } else {
                        html += '<div class="panel panel-default">' +
                            '      <div class="panel-body">' +
                            '           <div class="text-danger"> CPF não encontrado.</div>' +
                            '       </div>' +
                            '    </div>';
                        $('.print-result').html(html);
                    }
                }
            });

        });
    });
</script>
</body>
</html>